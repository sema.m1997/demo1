# -*- mode: ruby -*-
# vi: set ft=ruby :

#vagrant-hostmanager is a Vagrant plugin that manages the hosts file on guest machines
#vagrant-reload: Reload a VM in provisioning step.
#vagrant-faster: Allocates more CPUs based on machine capacity
if ARGV[0] != 'plugin'
    
   required_plugins = ['vagrant-hostmanager', 'vagrant-reload', 'vagrant-faster'
   ]
   plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
   if not plugins_to_install.empty?
     puts "Installing plugins: #{plugins_to_install.join(' ')}"
       if system "vagrant plugin install #{plugins_to_install.join(' ')}"
         exec "vagrant #{ARGV.join(' ')}"
       else
         abort "Installation of one or more plugins has failed. Aborting."
       end

   end
end

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what

#create user and sign in
$initScript = <<-SCRIPT
  
  sudo useradd -s /bin/bash -d /home/app_user/ -m -G sudo app_user
  sudo su - app_user
  cd /home/app_user
  export SCRIPT_HOME=/vagrant
  echo _______________________________________________________________________________
  echo 1. Install and setup java
  $SCRIPT_HOME/app.sh

SCRIPT

#Create petclinic service
$systemd = <<-SCRIPT
cat > /etc/systemd/system/petclinic.service <<EOF
[Unit]
Description=Petclinic Java Spring Boot
[Service]
User=app_user
Environment="MYSQL_PASS=123456@"
Environment="MYSQL_USER=samadb"
Environment="MYSQL_URL=jdbc:mysql://db:3306/samadb"

WorkingDirectory=/home/app_user/demo1
ExecStart=/bin/java -Xms128m -Xmx256m -Dspring.profiles.active=mysql -jar 
/home/app_user/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar

SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target

EOF

  sudo systemctl daemon-reload
  sudo systemctl enable petclinic.service
  sudo systemctl start petclinic

SCRIPT

Vagrant.configure("2") do |config|
  config.vm.boot_timeout = 1800
  #config.ssh.connect_timeout = 1800

   if Vagrant.has_plugin?("vagrant-hostmanager")
      config.hostmanager.enabled = true
      config.hostmanager.manage_host = true
      config.hostmanager.ignore_private_ip = false
      config.hostmanager.include_offline = true
   end

  config.vm.define :DB_VM do |db|
     db.vm.box = "ubuntu/focal64"
     db.vm.hostname = "db"
     #Run script
     db.vm.provision :shell, path: "db.sh"
     #Set private IP
     db.vm.network :private_network, ip: "192.168.33.11"
     db.vm.network "forwarded_port", guest: 3306, host: 8081,
     auto_correct: true
     db.vm.provider "virtualbox" do |vb|
       vb.name = "DB_VM"
       
       vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
       vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
       vb.customize ["modifyvm", :id, "--ioapic", "on"]
       vb.memory = "2048"
       vb.cpus = "3"
     end
  end

  config.vm.define :APP_VM do |app|
     app.vm.box = "ubuntu/focal64"
     app.vm.hostname = "app"
     
     app.vm.provision "shell", inline: $initScript
     app.vm.provision "shell", inline: $systemd
       #Set IP
     app.vm.network :private_network, ip: "192.168.33.12"
     app.vm.network "forwarded_port", guest: 80, host: 8082,
     auto_correct: true 
     app.vm.usable_port_range = 8000..8999
     app.vm.provider "virtualbox" do |vb|
        vb.name = "APP_VM"
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        vb.customize ["modifyvm", :id, "--ioapic", "on"]
        vb.memory = "2048"
        vb.cpus = "3"
     end
     #app.vm.provision :shell, path: "app.sh"
  end

  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  #config.vm.box = "ubuntu/focal64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
