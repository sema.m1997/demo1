#!/usr/bin/env bash

#First: Update apt base and install java
sudo apt-get update --fix-missing -y && sudo apt-get install openjdk-14-jdk -y

#Clone repo to machine
function clone_pull {
    Dir=$(basename "$1" .git)
    if [[ -d "$Dir" ]]; then
      cd $Dir
      git pull
    else
      git clone "$1" && cd $Dir
    fi
}

clone_pull https://gitlab.com/sema.m1997/demo1.git

#Set mvnw executable
chmod +x mvnw

#Run clean package
./mvnw clean package

#Copy target file user home folder
cp /home/app_user/demo1/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar /home/app_user/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar
