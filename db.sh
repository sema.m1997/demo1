#!/usr/bin/env bash

#Set env variables

DBNAME=samadb
DBUSER=samadb
DBPASS=123456@
ROOTPASS=123456@

apt-get update --fix-missing -y

debconf-set-selections <<< "mysql-server mysql-server/root_password password $ROOTPASS"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $ROOTPASS"

apt-get -y install mysql-server

#Create database, user and grant permissions 

mysql -uroot -p$ROOTPASS -e "DROP DATABASE $DBNAME;"
mysql -uroot -p$ROOTPASS -e "DROP USER $DBUSER;"
mysql -uroot -p$ROOTPASS -e "CREATE DATABASE $DBNAME;"
mysql -uroot -p$ROOTPASS -e "CREATE USER '$DBUSER'@'192.168.33.%' IDENTIFIED BY '$DBPASS';"
mysql -uroot -p$ROOTPASS -e "GRANT ALL ON $DBNAME.* TO '$DBUSER'@'192.168.33.%';"
readonly Q1="FLUSH PRIVILEGES;"
readonly SQL="${Q1}"
mysql -uroot -p$ROOTPASS -e "$SQL"

#Set bind address for connecting outside local
sudo sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

sudo service mysql restart
